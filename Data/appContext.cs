﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using cadastrodeprodutos.Models.DB;

#nullable disable

namespace cadastrodeprodutos.Data
{
    public partial class appContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Carro> Carros { get; set; }
        public DbSet<Contato> Contatos { get; set; }
        
        public appContext()
        {
        }

        public appContext(DbContextOptions<appContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=app.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}