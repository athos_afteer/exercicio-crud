using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using cadastrodeprodutos.Models.DB;
using cadastrodeprodutos.Data;

namespace cadastrodeprodutos.Controllers
{
    public class ProdutoController : Controller
    {
        private readonly ILogger<ProdutoController> _logger;

        private readonly appContext _context;

        public ProdutoController(ILogger<ProdutoController> logger, appContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            IList<Produto> produtos = _context.Produtos.ToList();
            return View(produtos);
        }
        public IActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Novo([Bind("Id,Nome,Valor,Estoque")] Produto novoProduto)
        {
            _context.Produtos.Add(novoProduto);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}